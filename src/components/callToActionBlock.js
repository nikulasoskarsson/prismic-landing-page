import React from "react"
import RichText from "./richText"
import styled from "styled-components"
import { Link } from "gatsby"

const StyledCallToActionBlockWrapper = styled.div`
  padding: 20px;
  background: #eee;
  padding: 20px;
  margin: 20px 0;
  border-radius: 10px;

  .call-to-action-content {
    display: flex;
    .featured-image-wrapper {
      margin: 0 10px;
      background: #fff;
      padding: 10px;
      border-radius: 10px;
      margin: auto 10px;
    }
    img {
      max-width: 100px;
      margin: 0;
    }
  }
`

const StyledButton = styled.div`
  background-color: orangered;
  display: inline-block;
  border-radius: 4px;

  cursor: pointer;

  a {
    color: #fff;
    padding: 4px 8px;
    display: inline-block;
    text-deceration: none;
  }
`

const CallToActionBlock = ({
  title,
  content,
  buttonLabel,
  buttonDestination,
  featuredImage,
}) => {
  return (
    <StyledCallToActionBlockWrapper>
      <div className="call-to-action-content">
        <RichText render={title} /> <RichText render={content} />
        <div className="featured-image-wrapper">
          <img src={featuredImage} alt="Featured" />
        </div>
      </div>

      <StyledButton>
        <Link to={buttonDestination}>{buttonLabel}</Link>
      </StyledButton>
    </StyledCallToActionBlockWrapper>
  )
}

export default CallToActionBlock
