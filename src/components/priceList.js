import React from "react"
import RichText from "./richText"
import styled from "styled-components"
import PriceItem from "./priceItem"

const StyledPriceListWrapper = styled.section`
  max-width: 800px;
  margin: 0 auto;

  > div:last-child {
    display: flex;
  }
`

const PriceList = ({ title, prices }) => {
  return (
    <StyledPriceListWrapper>
      <RichText render={title} />
      <div>
        {prices.map((price, i) => (
          <PriceItem
            mostPopular={price.price_type === "Most popular"}
            key={i}
            title={price.price_list_title}
            description={price.price_list_description}
            price={price.price_per_month}
          />
        ))}
      </div>
    </StyledPriceListWrapper>
  )
}

export default PriceList
