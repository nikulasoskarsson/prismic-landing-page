import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql, Link } from "gatsby"
import styled from "styled-components"

import "./layout.css"

const StyledMain = styled.main`
  margin: 0 auto;
`
const StyledLink = styled.div`
  margin: auto 0;

  a {
    color: #fff;
    padding: 0 16px;
    text-decoration: none;
    font-weight: bold;
    font-size: 16px;

    &:hover {
      color: orangered;
    }
  }
`
const StyledHeader = styled.header`
  display: flex;
  background: #444;
  height: 66px;
  padding: 0 16px;
  box-sizing: border-box;
`
const StyledNavContainer = styled.div`
  margin-left: auto;
  display: flex;
`

const StyledBranding = styled.div`
  color: orange;
  font-weight: bold;
  margin: auto 0;
  font-size: 20px;
`

const navigationQuery = graphql`
  {
    prismic {
      allNavigations {
        edges {
          node {
            branding
            navigation_links {
              label
              link {
                ... on PRISMIC_Page {
                  _meta {
                    uid
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`

const Layout = ({ children }) => {
  return (
    <>
      <StyledHeader>
        <StaticQuery
          query={navigationQuery}
          render={data => {
            console.log(data)

            return (
              <>
                <StyledBranding>
                  {data.prismic.allNavigations.edges[0].node.branding}
                </StyledBranding>
                <StyledNavContainer>
                  {data.prismic.allNavigations.edges[0].node.navigation_links.map(
                    link => (
                      <StyledLink key={link.link._meta.uid}>
                        <Link to={`/${link.link._meta.uid}`}>{link.label}</Link>
                      </StyledLink>
                    )
                  )}
                  }
                </StyledNavContainer>
              </>
            )
          }}
        />
      </StyledHeader>
      <StyledMain>{children}</StyledMain>>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
