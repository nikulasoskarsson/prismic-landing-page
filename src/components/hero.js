import React from "react"
import RichText from "./richText"
import styled from "styled-components"

const StyledHeroWrapper = styled.section`
    background: url('${({ backgroundImage }) => backgroundImage}');
    background-size:cover;
    background-repeat:no-repeat;
    height: calc(100vh - 66px);
    display:flex;
    align-items:center;
    text-align:center;
    color: #fff;
    div{
        max-width:800px;
        margin: 0 auto;
        position:relative;
        
        padding:5rem 8rem;
        background:rgba(0,0,0,0.5);
    }`

const Hero = ({ title, content, backgroundImage }) => {
  return (
    <StyledHeroWrapper backgroundImage={backgroundImage}>
      <div>
        <RichText render={title} />
        <p>{content}</p>
      </div>
    </StyledHeroWrapper>
  )
}

export default Hero
