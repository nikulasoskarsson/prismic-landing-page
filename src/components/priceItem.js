import React from "react"
import RichText from "./richText"
import styled from "styled-components"

const StyledPriceItemWrapper = styled.div`
  flex-grow: 1;
  flex-basis: 0;
  margin: 0 10px;
  background: ${({ mostPopular }) => (mostPopular ? "orange" : "#eee")};
  color: ${({ mostPopular }) => (mostPopular ? "#fff" : "#444")}}
  padding: 10px;
  position:relative;
  
  .most-popular{
    position:absolute;
    right:0;
    right:0;
    padding:5px;
    background:green;
    color:white;
    font-weight:bold;
  }

  .price {
    text-align: center;
    background: rgba(0, 0, 0, 0.2);
    font-size: 30px;
    padding: 10px;
    margin-left: -10px;
    margin-right: -10px;
    .duration {
      font-size: 16px;
    }
    .description {
      margin-top: 20px;
    }
  }
`

const PriceItem = ({ mostPopular, title, price, description }) => {
  return (
    <StyledPriceItemWrapper mostPopular={mostPopular}>
      {mostPopular && <div className="most-popular">Most popular</div>}
      <RichText render={title} />
      <div className="price">
        ${price}
        <span className="duration">/ month</span>
      </div>
      <RichText render={description} className="description" />
    </StyledPriceItemWrapper>
  )
}

export default PriceItem
