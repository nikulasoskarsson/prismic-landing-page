import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import styled from "styled-components"
import RichText from "../components/richText"

export const query = graphql`
  {
    prismic {
      allContact_pages {
        edges {
          node {
            form_title
            form_description
            form_fields {
              field_type
              name
              required
            }
          }
        }
      }
    }
  }
`
const StyledForm = styled.form`
  padding: 10px;
  background: #eee;
  margin-top: 20px;
  max-width: 800px;
  margin-left: auto;
  margin-right: auto;
`
const StyledButton = styled.button`
  background: orange;
  color #fff;
  cursor:pointer;
  padding: 4px 8px;
  box-shadow:none;
  border-radius:4px;
`

const ContactUs = props => {
  const formFields =
    props.data.prismic.allContact_pages.edges[0].node.form_fields
  console.log(formFields)

  const formTitle = props.data.prismic.allContact_pages.edges[0].node.form_title

  const formDescription =
    props.data.prismic.allContact_pages.edges[0].node.form_description

  return (
    <Layout>
      <RichText render={formTitle} />
      <RichText render={formDescription} />
      <StyledForm
        name="contact-us"
        method="POST"
        data-netlify="true"
        action="/contact-success"
      >
        <input type="hidden" value="contact-us" name="form-name" />
        {formFields.map((field, i) => {
          if (field === "textarea") {
            return (
              <div key={i}>
                <textarea
                  required={field.required === "Yes"}
                  placeholder={field.name}
                />
              </div>
            )
          } else {
            return (
              <div key={i}>
                <input
                  placeholder={field.name}
                  required={field.required === "Yes"}
                  type={field.field_type}
                />
              </div>
            )
          }
        })}
        <StyledButton type="submit">Submit</StyledButton>
      </StyledForm>
    </Layout>
  )
}

export default ContactUs
